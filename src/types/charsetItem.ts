export interface CharsetItem {
    type: string,
    childrens?: CharsetItem[],
    options?: {
        position: 'horizontal' | 'vertical'
    }
}
