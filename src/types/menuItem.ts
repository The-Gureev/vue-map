export interface MenuItem {
    component: string,
    title: string,
    subTitle: string
}
