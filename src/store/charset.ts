import { ref } from 'vue';

import { CharsetItem } from '../types';

export const charset = ref<CharsetItem>({
  type: 'Splitpanes',
  childrens: [
    { type: 'TreeLayers' }, { type: 'Map' },
  ],
  options: {
    position: 'horizontal',
  },
});
