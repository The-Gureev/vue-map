import { ref } from 'vue';

import { MenuItem } from '../types';

export const menuComponents = ref<MenuItem[]>([
  {
    component: 'Map',
    title: 'Карта',
    subTitle: 'Карта и инструменты работы с ней',
  },
  {
    component: 'TreeLayers',
    title: 'Слои',
    subTitle: 'Дерево доступных слоёв',
  },
  {
    component: 'CesiumMap',
    title: 'Cesium',
    subTitle: 'Карта Cesium',
  }]);
